import {BaseSettings} from "rdv-lib";
import {viewer} from '@env_temp/viewer';
import {richtextEditor} from "@env_temp/richtexteditor";

const main_url =  "https://ub-rdv-proxy.ub.unibas.ch"

export const environment: BaseSettings = {
  ...viewer,
  ...richtextEditor,
  production: true,
  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl: main_url + "/v1/rdv_query/es_proxy/",
  moreProxyUrl: main_url + "/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: main_url + "/v1/rdv_query/facet_search/",
  popupQueryProxyUrl: main_url + "/v1/rdv_query/popup_query/",
  documentViewerProxyUrl: main_url + "/v1/rdv_query/iiif_flex_pres/",
  detailProxyUrl: main_url + "/v1/rdv_object/object_view/",
  navDetailProxyUrl: main_url + "/v1/rdv_query/next_objectview/",
  detailSuggestionProxyUrl: main_url + "/v1/rdv_query/form_query/",
  suggestSearchWordProxyUrl: main_url + "/v1/rdv_query/autocomplete/",
};
