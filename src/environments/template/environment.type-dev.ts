import {BaseSettings} from "rdv-lib";
import {viewer} from '@env_temp/viewer';
import {richtextEditor} from "@env_temp/richtexteditor";

export const environment: BaseSettings = {
  ...viewer,
  ...richtextEditor,
  production: false,

  // creatable: [
  //   {
  //     label: {
  //       "de": "retrodigitalisierter Zeitungsausschnitt",
  //       "en": "scanned press clipping"
  //     },
  //     value: "dizas"
  //   },
  //   {
  //     label: {
  //       "de": "elektronischer Zeitungsausschnitt (ab 2013)",
  //       "en": "digitial press clipping (since 2013)"
  //     },
  //     value: "ezas"
  //   }
  // ],

  editable: true,
  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/es_proxy/",
  moreProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/facet_search/",
  popupQueryProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/popup_query/",
  documentViewerProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/iiif_flex_pres/",
  detailProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_object/object_view/",
  detailEditProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_object/object_edit/",
  navDetailProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/",
  detailSuggestionProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/form_query/",
  suggestSearchWordProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/autocomplete/",
  detailNewProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_object/object_new/"
};
