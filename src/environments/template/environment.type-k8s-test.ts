import {BaseSettings} from "rdv-lib";
import {viewer} from '@env_temp/viewer';
import {richtextEditor} from "@env_temp/richtexteditor";

export const environment: BaseSettings = {
  ...viewer,
  ...richtextEditor,
  production: true,

  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl: "https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_query/es_proxy/",
  moreProxyUrl: "https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_query/facet_search/",
  popupQueryProxyUrl: "https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_query/popup_query/",
  documentViewerProxyUrl: "https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_query/iiif_flex_pres/",
  detailProxyUrl: "https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_object/object_view/",
  navDetailProxyUrl: "https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_query/next_objectview/",
  detailSuggestionProxyUrl: "https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_query/form_query/",
  suggestSearchWordProxyUrl: "https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_query/autocomplete/",
};
