import {environment as prod} from "@env_temp/environment.type-prod";

const Proj = "tinti-test";

import {initRdvLib, SettingsModel} from 'rdv-lib'
import {environment as proj} from '@env/tinti/environment';
import {environment as test} from '@env_temp/environment.type-test';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...test,
  ...proj,

  headerSettings: addTestNamePostfix(proj.headerSettings),
  proxyUrl : test.proxyUrl +  Proj + "/",
  moreProxyUrl: test.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: test.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: test.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: test.navDetailProxyUrl +  Proj + "/",
};
initRdvLib(environment);
