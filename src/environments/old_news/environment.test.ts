import {environment as dev} from "@env_temp/environment.type-dev";

const Proj = "old_news-test";
import {initRdvLib, SettingsModel} from 'rdv-lib';
import {environment as proj} from '@env/old_news/environment';
import {environment as test} from '@env_temp/environment.type-test';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...test,
  headerSettings: addTestNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,

  proxyUrl : test.proxyUrl +  Proj + "/",
  moreProxyUrl: test.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: test.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: test.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: test.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: test.popupQueryProxyUrl +  Proj + "/",
};

initRdvLib(environment);
