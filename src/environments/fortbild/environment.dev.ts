import {environment as loc} from "@env_temp/environment.type-loc";

const Proj = "fortbild-dev";
import {initRdvLib, SettingsModel} from 'rdv-lib';
import {environment as proj} from '@env/fortbild/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...dev,
  headerSettings: addDevNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,

  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: dev.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: dev.suggestSearchWordProxyUrl +  Proj + "/",
  detailEditProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_object/object_edit/" +  Proj + "/",
};

initRdvLib(environment);
