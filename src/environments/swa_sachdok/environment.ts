import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType, FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  SettingsModel,
  SortOrder
} from "rdv-lib";

import {environment as swa} from '@env/swa/environment';


export const environment: SettingsModel = {
  ...swa,
  showImagePreview: false,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: true,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },
  facetFields: {
    //...swa.facetFields,
    "fuv": {
      "field": "fuv.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.descr_fuv",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "Type": {
      "field": "type_id.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Kleinschrift Typ",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10
    },
    "310a": {
      "field": "310a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Erscheinungshäufigkeit",
      "operator": "OR",
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "sys_created": {
      "field": "sys_created",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Letzter PDF Upload",
      "operator": "AND",
      "showAggs": true,
      "order": 10,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Anzahl PDFs": {
      "field": "count_pdfs",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Anzahl PDFs",
      "operator": "AND",
      "showAggs": true,
      "order": 1021,
      "size": 100,
      "expandAmount": 10
    } as HistogramFieldModel,
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "sys_created",
      order: SortOrder.ASC,
      display: "Upload (älteste)"
    },
    {
      field: "sys_created",
      order: SortOrder.DESC,
      display: "Upload (neueste)"
    }
  ],

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [20, 50, 100, 200],

  i18n: {
    "de": {
      "env.facetFields.fct_pubyear": "Erscheinungsjahr",
      "env.facetFields.fct_access": "Zugänglichkeit",
      "env.facetFields.fct_objecttype": "Objekttyp",
      "env.facetFields.fct_author": "Verfasser/in",
      "env.facetFields.descr_han_hierarchy_han_filter": "Bestandsgliederung",
      "env.facetFields.descr_han_hierarchy_per_filter": "Bestandsgliederung Nachlässe",
      "env.facetFields.descr_han_hierarchy_verband_filter": "Bestandsgliederung Verbände",
      "top.headerSettings.name": "SWA PDF Kleinschriften Archiv",
      "top.headerSettings.name.Dev": "SWA PDF Kleinschriften Archiv (Dev)",
      "top.headerSettings.name.Loc": "SWA PDF Kleinschriften Archiv (Loc)",
      "top.headerSettings.name.Test": "SWA PDF Kleinschriften Archiv (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    },
    "en": {
      "env.facetFields.fct_pubyear": "Year of publication",
      "env.facetFields.fct_access": "Accessibility",
      "env.facetFields.fct_objecttype": "Objekttyp",
      "env.facetFields.fct_author": "Author",
      "top.headerSettings.name": "SWA PDF Kleinschriften Archiv",
      "top.headerSettings.name.Dev": "SWA PDF Kleinschriften Archiv (Dev)",
      "top.headerSettings.name.Loc": "SWA PDF Kleinschriften Archiv (Loc)",
      "top.headerSettings.name.Test": "SWA PDF Kleinschriften Archiv (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    }
  }
}
initRdvLib(environment);
