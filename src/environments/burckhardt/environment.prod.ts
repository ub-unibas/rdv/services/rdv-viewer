import {environment as test} from "@env_temp/environment.type-test";

const Proj = "burckhardt-prod";


import {environment as proj} from '@env/burckhardt/environment';
import {environment as prod} from '@env_temp/environment.type-prod';
import {initRdvLib, SettingsModel} from "rdv-lib";

export const environment: SettingsModel = {
  ...prod,
  ...proj,


  proxyUrl : prod.proxyUrl +  Proj + "/",
  moreProxyUrl: prod.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: prod.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: prod.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: prod.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: prod.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: prod.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: prod.suggestSearchWordProxyUrl +  Proj + "/",
};
initRdvLib(environment);
