import {environment as test} from "@env_temp/environment.type-test";

const Proj = "bibnetz-loc";
import {initRdvLib, SettingsModel} from 'rdv-lib'
import {environment as proj} from "@env/bibnetz/environment";
import {environment as loc} from "@env_temp/environment.type-loc";
import {addLocNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...loc,
  headerSettings: addLocNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,

  proxyUrl : loc.proxyUrl +  Proj + "/",
  moreProxyUrl: loc.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: loc.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: loc.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: loc.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: loc.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: loc.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: loc.suggestSearchWordProxyUrl +  Proj + "/",
  detailEditProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_edit/" +  Proj + "/",
};

initRdvLib(environment);
