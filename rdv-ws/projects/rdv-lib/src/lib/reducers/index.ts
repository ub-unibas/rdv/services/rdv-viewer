/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ActionReducerMap, createFeatureSelector, createSelector, MetaReducer} from '@ngrx/store';
import {environment} from '../shared/Environment';
import * as fromRemoteFilterConfigs from '../core/reducers/remote-filter-configs.reducer';
import {memoize} from '../shared/utils';


/**
 * @ignore
 */
export interface State {
  userConfig: fromRemoteFilterConfigs.State;
}

/**
 * @ignore
 */
export let reducers: ActionReducerMap<State>;


/**
 * (De)activates meta reducers for production mode
 */
export let metaReducers: MetaReducer<State>[];


export function initCoreReducers(): void {
  reducers = {
    userConfig: fromRemoteFilterConfigs.reducer,
  };
  metaReducers = !environment.production ? [] : [];
}

/**
 * Feature selector for remotely fetched filter configuration
 */
export const getUserConfigState = createFeatureSelector<State, fromRemoteFilterConfigs.State>(
  'userConfig'
);

/**
 * Selector for fields in remotely fetched filter configuration
 */
export const getFilterFields = createSelector(
  getUserConfigState,
  (userConfig) => userConfig.filterFields,
);

/**
 * Gets remotely fetched filter fields by key
 */
export const getFilterFieldsByKey = createSelector(
  getFilterFields,
  (filterFields) => memoize((key: string) => filterFields[key]),
);
