/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {map, tap} from 'rxjs/operators';
import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BasketRequestInterceptor implements HttpInterceptor {

  private _cachedDocuments = {};

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.method === 'POST' &&
      req.headers.has('X-Request-Type') &&
      req.headers.get('X-Request-Type') === 'basket') {
      const [newRequest, cachedDocuments] = this._get(req);
      if (!newRequest) {
        return of(cachedDocuments);
      } else if (cachedDocuments.length === 0) {
        return next.handle(req).pipe(tap(event => {
          if (event instanceof HttpResponse) {
            this._put(event)
          }
        }));
      } else if (cachedDocuments && newRequest) {
        return next.handle(req).pipe(tap(event => {
          if (event instanceof HttpResponse) {
            this._put(event)
          }
        }), map(event => {
            if (event instanceof HttpResponse) {
              return new HttpResponse<any>({
                body: {
                  ...event.body,
                  response: {
                    ...event.body.response,
                    docs: event.body.snippets.concat(cachedDocuments),
                  }
                },
                headers: event.headers,
                status: event.status,
                statusText: event.statusText,
                url: event.url
              })
            } else {
              return event;
            }
          }
        ));
      } else {
        return next.handle(req);
      }
    } else {
      return next.handle(req);
    }
  }

  private _get(req: HttpRequest<any>): [HttpRequest<any>, any] {
    const parsedBody = JSON.parse(req.body);
    const newIds = [];
    const cachedDocuments = [];
    let httpRequest;
    for (const id of parsedBody.ids) {
      if (id in this._cachedDocuments) {
        cachedDocuments.push(this._cachedDocuments[id])
      } else {
        newIds.push(id);
      }
    }
    if (newIds) {
      httpRequest = new HttpRequest('POST', req.url, {
        ...parsedBody, ids: parsedBody.ids.concat(newIds),
      });
    }
    return [httpRequest, cachedDocuments];
  }

  private _put(resp: HttpResponse<any>): void {
    for (const doc of resp.body.snippets) {
      this._cachedDocuments[doc.id] = doc;
    }
  }
}
