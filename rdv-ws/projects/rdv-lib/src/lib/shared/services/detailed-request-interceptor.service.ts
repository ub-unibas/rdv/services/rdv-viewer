/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DetailedRequestInterceptor implements HttpInterceptor {

  private _documentCache = {};

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.method === 'POST' &&
      req.headers.has('X-Request-Type') &&
      req.headers.get('X-Request-Type') === 'detailed') {
    } else {
      return next.handle(req);
    }

    const cachedResponse = this._get(req);

    return cachedResponse ? of(cachedResponse) : next.handle(req).pipe(tap(event => {
      if (event instanceof HttpResponse) {
        this._put(req, event);
      }
    }));
  }

  private _get(req: HttpRequest<any>): HttpResponse<any> | void {
    const parsedBody = JSON.parse(req.body);
    if (parsedBody.ids[0] in this._documentCache) {
      return new HttpResponse<any>({body: this._documentCache[parsedBody.ids[0]]});
    }
  }

  private _put(req: HttpRequest<any>, resp: HttpResponse<any>): void {
    const parsedReqBody = JSON.parse(req.body);
    this._documentCache[parsedReqBody.ids[0]] = resp.body;
  }
}
