/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {map} from 'rxjs/operators';
import {environment} from '../Environment';

import {Injectable} from '@angular/core';

import {Observable} from "rxjs";
import {QueryFormat} from "../models/query-format";
import {Basket} from '../../search/models/basket.model';
import {HttpClient} from '@angular/common/http';
import {BackendSearchService} from './backend-search.service';
import {SettingsModel, SortOrder} from "../models/settings.model";
import {TranslateService} from "@ngx-translate/core";
import {FacetFieldModel, ListType} from "../models/settings.model";
import {AutoCompleteValue, EditDocField, LocalizedLabel} from "../models/doc-details.model";

@Injectable()
export class ElasticBackendSearchService extends BackendSearchService {

  //URL auf PHP-Proxy
  private proxyUrl: string;
  private suggestSearchWordProxyUrl: string;
  private moreProxyUrl: string;
  private detailProxyUrl: string;
  private detailEditProxyUrl: string;
  private detailNewProxyUrl: string;
  private detailSuggestionProxyUrl: string;
  private navDetailProxyUrl: string;
  //URL auf PHP-Proxy-Endpunkt für Suche innerhalb von Facetten
  private inFacetSearchProxyUrl: string;
  //URL auf PHP-Proxy-Endpunkt für Suche mit zusätzlichem Popup Info.
  private popupQueryProxyUrl: string;
  //Felder, die bei normaler Suche fuer die Treffertabelle geholt werden
  private tableFields: string[] = [];

  //Felder, die bei der Detailsuche geholt werden
  private detailFields: string[] = [];

  //Http Service injekten
  constructor(private http: HttpClient, protected translate: TranslateService) {

    super();

    //Main-Config laden
    const mainConfig: SettingsModel = environment;

    //proxyUrls setzen
    this.proxyUrl = mainConfig.proxyUrl;
    this.suggestSearchWordProxyUrl = mainConfig.suggestSearchWordProxyUrl;
    this.moreProxyUrl = mainConfig.moreProxyUrl;
    this.inFacetSearchProxyUrl = mainConfig.inFacetSearchProxyUrl;
    this.detailProxyUrl = mainConfig.detailProxyUrl;
    this.detailEditProxyUrl = mainConfig.detailEditProxyUrl;
    this.detailSuggestionProxyUrl = mainConfig.detailSuggestionProxyUrl;
    this.navDetailProxyUrl = mainConfig.navDetailProxyUrl;
    this.popupQueryProxyUrl = mainConfig.popupQueryProxyUrl;
    this.detailNewProxyUrl = mainConfig.detailNewProxyUrl;

    //Felder sammeln, die bei normaler Suche geholt werden sollen
    for (const field of mainConfig.tableFields) {

      //ID Feld kommt sowieso (allerdings als _id und nicht als id in _source), daher id nicht in Liste der geholten Felder einfuegen
      if (field.field !== "id") {

        //Feld-Namen in tempArray sammeln
        this.tableFields.push(field.field);
      }
    }

    //Felder sammeln, die bei Detailsuche geholt werden sollen
    for (const key of Object.keys(mainConfig.extraInfos)) {

      //Feld-Namen in tempArray sammeln
      this.detailFields.push(mainConfig.extraInfos[key].field);
    }
  }

  protected jsonHttpClientOptions(xRequestType: string) {
    return {headers: {'Content-Type': 'application/json', 'X-Request-Type': xRequestType}, withCredentials: true};
  }

  getJsonSearchHttpClientOptions() {
    return this.jsonHttpClientOptions('search');
  }

  getJsonDetailedHttpClientOptions() {
    return this.jsonHttpClientOptions('detailed');
  }

  getJsonSaveDetailedHttpClientOptions() {
    return this.jsonHttpClientOptions('save-detail');
  }

  getJsonBasketHttpClientOptions() {
    return this.jsonHttpClientOptions('basket');
  }

  //Daten in Elasticsearch suchen
  getBackendDataComplex(queryFormat: QueryFormat): Observable<any> {

    //Anfrageobjekt erzeugen
    const complexQueryFormat = {};

    this.handleQueryParams(complexQueryFormat, queryFormat);

    //Ueber Anfrage-Formate gehen
    this.handleSearchFields(queryFormat, complexQueryFormat);

    this.handleTextSearch(complexQueryFormat);

    this.handleFilters(queryFormat, complexQueryFormat);

    this.handleSimpleFacets(queryFormat, complexQueryFormat);

    this.setRangeFacets(queryFormat, complexQueryFormat);

    this.setHistogramFacets(queryFormat, complexQueryFormat);

    this.setHierarchyFacets(queryFormat, complexQueryFormat);

    //Liste der zu holenden Tabellenfelder
    this.handleReturnFields(complexQueryFormat);

    this.handleLanguage(queryFormat, complexQueryFormat);

    this.setFacetSearchValues(queryFormat, complexQueryFormat);

    this.handleDocumentViewer(queryFormat, complexQueryFormat);

    if (queryFormat.return_selection) {
      complexQueryFormat["return_selection"] = queryFormat.return_selection;
    }

    if (queryFormat.persist) {
      complexQueryFormat["persist"] = queryFormat.persist;
    }

    // console.error("ES QUERY", JSON.stringify(complexQueryFormat, null, 2));

    //HTTP-Anfrage an Elasticsearch
    let url;
    if (queryFormat.popupId) {
      url = this.popupQueryProxyUrl;
      complexQueryFormat["popup_query"] = queryFormat.popupId;
    } else {
      url = (queryFormat.documentViewer !== ListType.LIST.toString()) ? environment.documentViewerProxyUrl : this.proxyUrl;
    }

    return this.http
      .post(url,
        JSON.stringify(complexQueryFormat),
        this.getJsonSearchHttpClientOptions())
      .pipe(map((response: any) => response));
  }

  suggestBackendSearchWord(queryFormat: QueryFormat): Observable<any> {
    //Anfrageobjekt erzeugen
    const complexQueryFormat = {};

    this.handleQueryParams(complexQueryFormat, queryFormat);

    //Ueber Anfrage-Formate gehen
    this.handleSearchFields(queryFormat, complexQueryFormat);

    this.handleTextSearch(complexQueryFormat);

    this.handleSimpleFacets(queryFormat, complexQueryFormat);

    this.setRangeFacets(queryFormat, complexQueryFormat);

    this.setHistogramFacets(queryFormat, complexQueryFormat);

    this.setHierarchyFacets(queryFormat, complexQueryFormat);

    this.handleReturnFields(complexQueryFormat);

    this.handleLanguage(queryFormat, complexQueryFormat);

    this.handleSuggestion(queryFormat, complexQueryFormat);

    this.handleSuggestedTerms(queryFormat, complexQueryFormat);

    return this.http
      .post(this.suggestSearchWordProxyUrl, JSON.stringify(complexQueryFormat), this.getJsonSearchHttpClientOptions())
      .pipe(map((response: any) => response));
  }

  suggestBackendDetailEditFieldData(id: string, field: string, query: string, selected: AutoCompleteValue[]): Observable<any> {
    const data = {field: field, query: query, object_id: id, added_entries: selected};
    return this.http
      .post(this.detailSuggestionProxyUrl,
        JSON.stringify(data),
        this.getJsonSearchHttpClientOptions())
      .pipe(map((response: any) => response));
  }

  // depends on handleQueryParams(), handleSimpleFacets()
  handleSuggestion(queryFormat: QueryFormat, complexQueryFormat) {
    const openFacets = {};
    complexQueryFormat['open_facets'] = openFacets;
    const facetSize = {};
    complexQueryFormat['query_params']['facet_size'] = facetSize;
    const facets = complexQueryFormat.facets;
    // only one search field is supported
    const searchfield_data = queryFormat.searchFields[Object.keys(queryFormat.searchFields)[0]];
    let searchedTerm = [];
    if (searchfield_data) {
      searchedTerm = searchfield_data.value;
    }

    for (const key of Object.keys(environment.facetFields)) {
      const facetData: FacetFieldModel = environment.facetFields[key];
      if (facetData.facetType === 'simple' && facetData.autocomplete_size > 0) {
        if (searchedTerm) {
          openFacets[facetData.field] = searchedTerm[0];
        }
        facetSize[key] = facetData.autocomplete_size;
        facets[key]["size"] = facetData.autocomplete_size;
      }
    }
  }

  handleSuggestedTerms(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.added_entries) {
      complexQueryFormat['added_entries'] = queryFormat.added_entries;
    }
  }

  getMoreBackendDataComplex(queryFormat: QueryFormat): Observable<any> {
    //Anfrageobjekt erzeugen
    const complexQueryFormat = {};

    this.handleQueryParams(complexQueryFormat, queryFormat);

    //Ueber Anfrage-Formate gehen
    this.handleSearchFields(queryFormat, complexQueryFormat);

    this.handleTextSearch(complexQueryFormat);

    this.handleFilters(queryFormat, complexQueryFormat);

    this.handleSimpleFacets(queryFormat, complexQueryFormat);

    this.setRangeFacets(queryFormat, complexQueryFormat);

    this.setHistogramFacets(queryFormat, complexQueryFormat);

    this.setHierarchyFacets(queryFormat, complexQueryFormat);

    this.handleSearchAfterParams(queryFormat, complexQueryFormat);

    this.handleLanguage(queryFormat, complexQueryFormat);

    this.setFacetSearchValues(queryFormat, complexQueryFormat);

    this.handleDocumentViewer(queryFormat, complexQueryFormat);

    //Liste der zu holenden Tabellenfelder
    this.handleReturnFields(complexQueryFormat);

    // console.log("ES MORE QUERY", JSON.stringify(complexQueryFormat, null, 2));

    //HTTP-Anfrage an Elasticsearch
    return this.http

    //POST Anfrage
      .post(this.moreProxyUrl,
        JSON.stringify(complexQueryFormat),
        this.getJsonSearchHttpClientOptions())
      .pipe(
        //Antwort als JSON weiterreichen
        map((response: any) => response));
  }

  private handleReturnFields(complexQueryFormat) {
    let esProxyQueryParams: any = complexQueryFormat['query_params'];
    if (!esProxyQueryParams) {
      complexQueryFormat['query_params'] = esProxyQueryParams = {};
    }
    esProxyQueryParams.source = this.tableFields;
  }

  private handleLanguage(queryFormat: QueryFormat | Basket, complexQueryFormat) {
    complexQueryFormat['lang'] = queryFormat.lang || "de";
  }

  private handleDocumentViewer(queryFormat: QueryFormat, complexQueryFormat) {
    complexQueryFormat['viewer'] = queryFormat.documentViewer;
  }

  //Suchparameter fuer Paging und Sortierung direkt aus Queryformat uebernehmen
  private handleQueryParams(complexQueryFormat, queryFormat: QueryFormat) {
    const queryParams = queryFormat.queryParams;
    if (queryParams) {
      let esProxyQueryParams: any = complexQueryFormat['query_params'];
      if (!esProxyQueryParams) {
        complexQueryFormat['query_params'] = esProxyQueryParams = {};
      }
      esProxyQueryParams.size = queryParams.rows;
      esProxyQueryParams.sort = [{
        field: (queryParams.sortField === "") ? [] : queryParams.sortField,
        order: queryParams.sortDir.toString()
      }];
      esProxyQueryParams.start = queryParams.start;
      complexQueryFormat['query_params'] = esProxyQueryParams;
    }
  }

  private handleSearchAfterParams(queryFormat: QueryFormat, complexQueryFormat) {
    complexQueryFormat['search_after_values'] = queryFormat.search_after;
    complexQueryFormat['search_after_order'] = queryFormat.queryParams.sortDir;
  }

  private handleSearchFields(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.searchFields) {
      for (const key of Object.keys(queryFormat.searchFields)) {

        //Schnellzugriff auf dieses Suchfeld
        const searchfield_data = queryFormat.searchFields[key];
        const searchfield_data_value = searchfield_data.value;

        //Wenn Wert gesetzt ist (z.B. bei der Titel-Suche)
        if (searchfield_data_value.length > 0) {

          //Wenn query-Bereich noch nicht angelegt wurde
          if (complexQueryFormat["query"] === undefined) {

            //Geruest einer Boolquery aufbauen (Titel:Freiheit AND Person:Martin He*), einzelne Suchfelder werden per AND verknuepft
            complexQueryFormat["query"] = {
              "bool": {
                "must": []
              }
            };
          }

          const queryString = {
            "query_string": {
              "query": searchfield_data_value,
              "op": "AND"
            }
          };

          //Wenn nicht in Freitext gesucht wird
          if (searchfield_data.field !== "all_text") {

            //Passendes Suchfeld setzen
            queryString["query_string"]["searchFields"] = [searchfield_data.field]
          }

          //Bool-Queries sammeln
          complexQueryFormat["query"]["bool"]["must"].push(queryString);
        }
      }
    }
  }

  private handleTextSearch(complexQueryFormat) {
    //Wenn es keine komplexe Suche gibt (also keine Werte in den Suchfeldern stehen)
    if (complexQueryFormat["query"] === undefined) {

      //match_all = *:* Anfrage-Parameter setzen fuer proxy
      complexQueryFormat["match_all"] = true;
    }
  }

  private ensureEsProxyFacetObject(complexQueryFormat): any {
    let esProxyFacetParams: any = complexQueryFormat['facets'];
    if (!esProxyFacetParams) {
      complexQueryFormat['facets'] = esProxyFacetParams = {};
    }
    return esProxyFacetParams;
  }

  private handleSimpleFacets(queryFormat: QueryFormat, complexQueryFormat) {
    const esProxyFacetParams: any = this.ensureEsProxyFacetObject(complexQueryFormat);
    if (queryFormat.facetFields) {
      for (const key of Object.keys(queryFormat.facetFields)) {

        //Schnellzugriff auf Infos dieser Facette
        // noinspection JSUnusedLocalSymbols
        const oldFacetData = queryFormat.facetFields[key];
        const facetData: any = {...oldFacetData};
        // map env fact type to elastic search query facet type
        // subcat maps to subcat
        if (oldFacetData.facet_type === 'simple') {
          facetData.facet_type = 'basic';
        }
        facetData.size = (environment.facetFields[key].size !== undefined) ? environment.facetFields[key].size : 10;

        //Infos zu Felder, benutzer Verknuepfung und ausgewaehlten Werten
        esProxyFacetParams[key] = facetData;
      }
    }
  }

  private handleFilters(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.filterFields) {
      for (const key of Object.keys(queryFormat.filterFields)) {

        //Schnellzugriff auf Infos dieses Filters
        const filterData = queryFormat.filterFields[key];

        //Ueber ausgewaehlte Filterwerte dieses Filters gehen (["Artikel", "Buch"] bei Filter "Typ")
        filterData.values.forEach((item, index) => {

          //Zu Beginn gibt es noch keinen Filterbereich -> anlegen
          if (complexQueryFormat["filter"] === undefined) {

            //Filterbereich mit must-clause-Array (AND-Verknuepfung) anlegen. Must query kombiniert die einzelnen Filter per AND.
            //Die einzelnen Werte eines Filters sind dann per OR verknuepft: type:Artikel AND ort:(Berlin OR Bremen)
            complexQueryFormat["filter"] = {
              "bool": {
                "must": []
              }
            };
          }

          //Bei 1. Filterwert ("Aritkel") dieses Filters ("Dokumentyp")
          if (index === 0) {

            //should-clause (=OR-Verknuepfung) anlegen fuer diesen Filter (Artikel OR Buch).
            //Wenn Filterung per AND (Region = Aushangsort AND Druckort), muss hier must statt should gesetzt werden
            complexQueryFormat["filter"]["bool"]["must"].push({"bool": {"should": []}});
          }

          //passenden Index im must-Array finden = letzter Eintrag
          const mustIndex = complexQueryFormat["filter"]["bool"]["must"].length - 1;

          //Filterwert in should-clause dieses Filteres einfuegen als term-query (exakter Treffer auf keyword-type)
          complexQueryFormat["filter"]["bool"]["must"][mustIndex]["bool"]["should"].push({"term": {[filterData.field]: item}});
        });
      }
    }
  }

  getInFacetSearchBackendData(queryFormat: QueryFormat): Observable<any> {
    //Anfrageobjekt erzeugen
    const complexQueryFormat = {};

    this.handleQueryParams(complexQueryFormat, queryFormat);

    this.handleSimpleFacets(queryFormat, complexQueryFormat);

    this.setHistogramFacets(queryFormat, complexQueryFormat);

    this.setHierarchyFacets(queryFormat, complexQueryFormat);

    //Ueber Anfrage-Formate gehen
    this.handleSearchFields(queryFormat, complexQueryFormat);

    this.handleTextSearch(complexQueryFormat);

    this.handleFilters(queryFormat, complexQueryFormat);

    this.setFacetSearchValues(queryFormat, complexQueryFormat);

    //Liste der zu holenden Tabellenfelder
    this.handleReturnFields(complexQueryFormat);

    this.handleLanguage(queryFormat, complexQueryFormat);

    // console.log("ES IN FACET QUERY", JSON.stringify(complexQueryFormat, null, 2)); // TODO

    //HTTP-Anfrage an Elasticsearch
    return this.http

      .post(this.inFacetSearchProxyUrl, JSON.stringify(complexQueryFormat), this.getJsonSearchHttpClientOptions())
      .pipe(map((response: any) => response));
  }

  /**
   * @Deprecated use histogramFields instead.
   *
   * @param {QueryFormat} queryFormat
   * @param complexQueryFormat
   */
  private setRangeFacets(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.rangeFields) {
      for (const key of Object.keys(queryFormat.rangeFields)) {

        //Zu Beginn gibt es noch keinen Rangebereich -> anlegen
        if (complexQueryFormat["range"] === undefined) {

          //Rangebereich = aggs anlegen und Infos sammeln (eigentliche Anfrage in php erstellt)
          complexQueryFormat["range"] = {};
        }

        //Rangeinfos weiterreichen
        complexQueryFormat["range"][key] = queryFormat.rangeFields[key];
      }
    }
  }

  /**
   * Add/replace histogram facets.
   *
   * @param {QueryFormat} queryFormat
   * @param complexQueryFormat
   */
  private setHistogramFacets(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.histogramFields) {
      const esProxyFacetParams: any = this.ensureEsProxyFacetObject(complexQueryFormat);

      for (const key of Object.keys(queryFormat.histogramFields)) {

        const oldFacetValue = queryFormat.histogramFields[key];
        const newFacetValue: any = {...oldFacetValue};
        newFacetValue.show_aggs = oldFacetValue.showAggs;
        delete newFacetValue.showAggs;
        newFacetValue.size = (environment.facetFields[key].size !== undefined) ? environment.facetFields[key].size : 10;
        newFacetValue.facet_type = oldFacetValue.data_type;
        delete newFacetValue.data_type;
        // user input uses from/to while backend facet values use gte/lte
        newFacetValue.values = oldFacetValue.values.map((v) => {
          const [from, to] = this.getHistogramRange(v);
          return {gte: from, lte: to};
        });
        esProxyFacetParams[key] = newFacetValue;
      }
    }
  }

  /**
   * Add/replace histogram facets.
   *
   * @param {QueryFormat} queryFormat
   * @param complexQueryFormat
   */
  private setHierarchyFacets(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.hierarchicFields) {
      const esProxyFacetParams: any = this.ensureEsProxyFacetObject(complexQueryFormat);

      for (const key of Object.keys(queryFormat.hierarchicFields)) {

        //Rangeinfos weiterreichen
        const oldFacetValue = queryFormat.hierarchicFields[key];
        const facetValue: any = {...oldFacetValue};
        facetValue.facet_type = "hierarchy";
        facetValue.size = (environment.facetFields[key].size !== undefined) ? environment.facetFields[key].size : 10;
        esProxyFacetParams[key] = facetValue;
      }
    }
  }

  protected setFacetSearchValues(queryFormat: QueryFormat, complexQueryFormat) {
    complexQueryFormat['open_facets'] = queryFormat.inFacetSearch;
  }

//Detail-Daten aus Elasticsearch fuer zusaetzliche Zeile holen (abstract,...)
  getBackendDetailData(id: any, fullRecord: boolean = false): Observable<any> {

    //Objekt fuer Detailsuche
    const detailQueryFormat = {};

    //ID-Suche (nach 1 ID)
    detailQueryFormat["ids"] = [id];

    //Wenn nur ein Teil der Felder geholt werden soll
    if (!fullRecord) {

      //Liste der zu holenden Felder
      detailQueryFormat["sourceFields"] = this.detailFields;
    }

    const url = this.detailProxyUrl + encodeURIComponent(id) + "/" + this.translate.currentLang;

    // console.log("getBackendDetailData", url, JSON.stringify(detailQueryFormat));

    //HTTP-Anfrage an Elasticsearch
    return this.http.get(url, this.getJsonDetailedHttpClientOptions());
  }

  //Detail-Edit-Daten aus Elasticsearch fuer zusaetzliche Zeile holen (abstract,...)
  getBackendDetailEditData(id: any, fullRecord: boolean = false): Observable<any> {

    //Objekt fuer Detailsuche
    const detailQueryFormat = {};

    //ID-Suche (nach 1 ID)
    detailQueryFormat["ids"] = [id];

    //Wenn nur ein Teil der Felder geholt werden soll
    if (!fullRecord) {

      //Liste der zu holenden Felder
      detailQueryFormat["sourceFields"] = this.detailFields;
    }

    const url = this.detailEditProxyUrl + encodeURIComponent(id) + "/" + this.translate.currentLang;

    // console.log("getBackendDetailData", url, JSON.stringify(detailQueryFormat));

    //HTTP-Anfrage an Elasticsearch
    return this.http.get(url, this.getJsonDetailedHttpClientOptions());
  }

  //Detail-New-Daten aus Elasticsearch fuer zusaetzliche Zeile holen (abstract,...)
  getBackendDetailNewData(doctype: string): Observable<any> {

    const url = this.detailNewProxyUrl + encodeURIComponent(doctype) + "/" + this.translate.currentLang;

    // console.log("getBackendDetailData", url, JSON.stringify(detailQueryFormat));

    //HTTP-Anfrage an Elasticsearch
    return this.http.get(url, this.getJsonDetailedHttpClientOptions());
  }

  saveBackendDetailEditData(object_id: string, data: EditDocField[], objectType?: string): Observable<any> {
    let url = this.detailEditProxyUrl;

    // don't cache save requests ... see SearchRequestInterceptor
    const paramSep = (url.indexOf("?") >= 0) ? "&" : "?";
    url += paramSep + "ic=1";

    // console.log("saveBackendDetailEditData", url, JSON.stringify(data));

    //HTTP-Anfrage an Elasticsearch
    const docData = {object_id: object_id, fields: data};
    if (objectType) {
      const otv: { value: string, label?: LocalizedLabel } = {value: objectType};
      // for new doc add label too (editing may be active, but creation of docs not)
      const allTypes = environment.creatable;
      if (allTypes) {
        for (const ot of allTypes) {
          if (ot.value === objectType) {
            otv.label = ot.label;
            break;
          }
        }
      }
      docData["object_type"] = otv;
    }
    return this.http
      .post(url, JSON.stringify(docData), this.getJsonSaveDetailedHttpClientOptions())
      .pipe(
        //Antwort als JSON weiterreichen
        map((response: any) => response))
  }

  navBackendDetailData(id: any, queryFormat: QueryFormat, next: boolean): Observable<any> {

    //Anfrageobjekt erzeugen
    const complexQueryFormat = {};

    this.handleQueryParams(complexQueryFormat, queryFormat);

    //Ueber Anfrage-Formate gehen
    this.handleSearchFields(queryFormat, complexQueryFormat);

    this.handleTextSearch(complexQueryFormat);

    this.handleFilters(queryFormat, complexQueryFormat);

    this.handleSimpleFacets(queryFormat, complexQueryFormat);

    this.setRangeFacets(queryFormat, complexQueryFormat);

    this.setHistogramFacets(queryFormat, complexQueryFormat);

    this.setHierarchyFacets(queryFormat, complexQueryFormat);

    complexQueryFormat['search_after_values'] = id;
    const inverseSearchDir = ((queryFormat.queryParams.sortDir === SortOrder.ASC.toString())
      ? SortOrder.DESC.toString() : SortOrder.ASC.toString());
    complexQueryFormat['search_after_order'] = next ? queryFormat.queryParams.sortDir : inverseSearchDir;

    //Liste der zu holenden Tabellenfelder
    this.handleReturnFields(complexQueryFormat);

    this.handleLanguage(queryFormat, complexQueryFormat);

    this.handleDocumentViewer(queryFormat, complexQueryFormat);

    // console.log("ES NAV DETAIL QUERY", JSON.stringify(complexQueryFormat, null, 2));

    //HTTP-Anfrage an Elasticsearch
    return this.http

    //POST Anfrage
      .post(this.navDetailProxyUrl, JSON.stringify(complexQueryFormat), this.getJsonSearchHttpClientOptions())
      .pipe(
        //Antwort als JSON weiterreichen
        map((response: any) => response));
  }

  //Merklisten-Daten in Elasticsearch suchen (ueber IDs)
  getBackendDataBasket(basket: Basket): Observable<any> {

    //Anfrage-Objekt erstellen
    const basketQueryFormat = {};

    //Parameter fuer Paging und Sortierung direkt vom Merklisten-Objekt uebernehmen
    basketQueryFormat["queryParams"] = basket.queryParams;

    //zu suchende IDs on Merklisten-Objekt uebernehmen
    basketQueryFormat["ids"] = basket.ids;

    //Liste der Tabellenfelder mitgeben
    basketQueryFormat['sourceFields'] = this.tableFields;
    //console.log(JSON.stringify(basketQueryFormat));

    this.handleLanguage(basket, basketQueryFormat);

    //HTTP-Anfrage an Elasticsearch
    return this.http

    //POST Anfrage mit URL, Liste der IDs und Liste der Felder
      .post(this.proxyUrl, JSON.stringify(basketQueryFormat), this.getJsonBasketHttpClientOptions())
      .pipe(
        //von JSON-Antwort nur die Dokument weiterreichen
        map((response: any) => response));
  }


  /**
   * @Override
   * @param {array} deepUrlFormat first element is the position, second the id
   * @returns {{id: string, pos: string}}
   */
  buildHierarchicValue(deepUrlFormat): any {
    return {id: deepUrlFormat[1], pos: deepUrlFormat[0]};
  }

  /**
   * @Override
   * @param hierarchicValue
   * @returns {[any, any]}
   */
  getDeepUrlHierarchicValue(hierarchicValue): [any, any] {
    return [hierarchicValue.pos, hierarchicValue.id];
  }

  /**
   * @Override
   * @returns {[number]}
   */
  getHistogramRange(selectedHistogramValue): [any, any] {
    return [
      selectedHistogramValue.gte,
      selectedHistogramValue.lte
    ];
  }
}
