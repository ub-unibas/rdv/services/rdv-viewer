import {Injectable} from "@angular/core";
import {ActiveToast, IndividualConfig, ToastrService} from "ngx-toastr";
import {LangChangeEvent, TranslateService} from "@ngx-translate/core";
import {Subscription} from "rxjs";

export interface LocalizedText {
  key: string;
  values?: { [key: string]: any };
}

@Injectable({
  providedIn: 'root'
})
export class I18nToastrService {
  private languageChangeSubscription: Subscription;

  constructor(protected toastr: ToastrService, protected translate: TranslateService) {
    this.languageChangeSubscription = this.translate.onLangChange.subscribe(
      (params: LangChangeEvent) => {
        this.languageChanged(params.lang);
      });
  }

  protected languageChanged(newLang: string) {
    const allToasts = this.toastr.toasts;
    let messageInfo, titleInfo;
    if (allToasts) {
      for (const at of allToasts) {
        messageInfo = (at as any)._rdvMessageKey;
        titleInfo = (at as any)._rdvTitleKey;
        const message = this.translateText(messageInfo);
        const title = this.translateText(titleInfo);
        if (message) {
          at.toastRef.componentInstance.message = message;
        }
        if (title) {
          at.toastRef.componentInstance.title = title;
        }
      }
    }
  }

  protected translateText(info: string | LocalizedText): string {
    if (!info) {
      return undefined;
    }
    let values = undefined;
    let key = undefined;
    if (typeof info === "string") {
      key = info;
    } else {
      key = (info as LocalizedText).key;
      values = (info as LocalizedText).values;
    }
    return this.translate.instant(key, values);
  }

  public info(messageInfo?: string | LocalizedText,
              titleInfo?: string | LocalizedText,
              override?: Partial<IndividualConfig>): ActiveToast<any> {
    const newToast = this.toastr.info(this.translateText(messageInfo), this.translateText(titleInfo), override);
    this.rememberI18nKeys(newToast, messageInfo, titleInfo);
    return newToast;
  }

  public error(messageInfo?: string | LocalizedText,
               titleInfo?: string | LocalizedText,
               override?: Partial<IndividualConfig>): ActiveToast<any> {
    const newToast = this.toastr.error(this.translateText(messageInfo), this.translateText(titleInfo), override);
    this.rememberI18nKeys(newToast, messageInfo, titleInfo);
    return newToast;
  }

  protected rememberI18nKeys(newToast, messageInfo: string | LocalizedText, titleInfo: string | LocalizedText) {
    (newToast as any)._rdvMessageKey = messageInfo;
    (newToast as any)._rdvTitleKey = titleInfo;
  }

  public clear(toastId?: number) {
    this.toastr.clear(toastId);
  }
}
