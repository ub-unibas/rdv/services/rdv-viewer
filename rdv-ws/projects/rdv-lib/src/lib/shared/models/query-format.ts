/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {FacetFieldType, FacetValueOrder, HistogramFieldType} from './settings.model';

export interface QueryParams {
  rows: number;
  start: number;
  sortField: string;
  sortDir: "" | "asc" | "desc",
}

// "key" is "search<n>" where n >= 0. Currently only 1 search field is supported.
export interface QueryFormatSearchFieldMap {
  [key: string]: {
    value: string[]
    field: string;
  }
}

export interface CommonQueryFormatFacet {
  size: number;
  field: string;
  operator: "OR" | "AND" | "NOT";
  facet_type: FacetFieldType;
  order: FacetValueOrder;
}

export interface QueryFormatFacetField extends CommonQueryFormatFacet {
  values: any[];
}

// "key" is a value from environment.ts's facetFields keys.
export interface QueryFormatFacetFieldMap {
  [key: string]: QueryFormatFacetField;
}

export interface QueryFormatHistogramFacetField extends CommonQueryFormatFacet {
  values: { from: any; to: any; }[];
  showAggs: boolean;
  data_type: HistogramFieldType;
}

// "key" is a value from environment.ts's facetFields keys.
export interface QueryFormatHistogramFacetFieldMap {
  [key: string]: QueryFormatHistogramFacetField;
}

export interface QueryFormatHierarchicFacetField extends CommonQueryFormatFacet {
  values: any[];
}

// "key" is a value from environment.ts's facetFields keys.
export interface QueryFormatHierarchicFacetFieldMap {
  [key: string]: QueryFormatHierarchicFacetField;
}

// "key" is a value from environment.ts's filterFields keys.
export interface QueryFormatFilterFieldMap {
  [key: string]: {
    field: string;
    values: any[];
  };
}

// "key" is a value from environment.ts's rangeFields keys.
export interface QueryFormatRangeFieldMap {
  [key: string]: {
    from: any;
    to: any;
    showMissingValues: boolean;
    field: string;
    min: any;
    max: any;
  }
}

/**
 * Query parameters. Used for communication with backend.
 */
export class QueryFormat {
  /**
   * Selected search fields.
   */
  searchFields: QueryFormatSearchFieldMap = {};
  /**
   * Selected facet fields
   */
  facetFields: QueryFormatFacetFieldMap = {};
  /**
   * Selected filters
   */
  filterFields: QueryFormatFilterFieldMap = {};
  /**
   * Selected ranges
   * @Deprecated use histogramFields instead
   */
  rangeFields: QueryFormatRangeFieldMap = {};
  /**
   * Selected histograms.
   */
  histogramFields: QueryFormatHistogramFacetFieldMap = {};
  /**
   * Selected hierarchic fields.
   */
  hierarchicFields: QueryFormatHierarchicFacetFieldMap = {};
  /**
   * Settings for result list
   */
  queryParams: QueryParams = {
    "rows": 10,
    "start": 0,
    "sortField": "",
    "sortDir": ""
  };

  /**
   * Currently active in-facet search prefixes
   */
  inFacetSearch?: { [key: string]: string };
  /**
   * set this field to load more docs for current search.
   */
  search_after?: any;

  /**
   * Set this flag to true if the search result shall be displayed in the
   * IIIF Universal viewer.
   */
  documentViewer?: string;
  /**
   * Run special search which is used for external links.
   */
  popupId?: { id: string; };
  /**
   * Current language: de | en
   */
  lang: string;
  /**
   * List of value ids already selected in autocomplete component.
   */
  added_entries?: string[];
  /**
   * For facet filters in URL, return their labels etc. too.
   */
  return_selection?: boolean;
  /**
   * Informs backend to store the corresponding IIF Universal Viewer manifest URL.
   */
  persist?: boolean;
}
