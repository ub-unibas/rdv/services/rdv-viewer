/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {FacetSelection} from "../../search/actions/result.actions";
import {PopupType} from "../../search/reducers/result.reducer";
import {Result} from "../../search/models/result.model";

/**
 * These interfaces describe the backend facet response format of search requests.
 */

export interface FacetResponse {
  label: string;
  value: { id?: any; type?: string; pos?: string; lte?: string; gte?: string; };
  count: number;
  sub_values?: FacetResponse[];
  pos?: string;
  // internal runtime fields:
  id?: string;
}

// "key" is a "field" in environment.ts' facetFields
export interface FacetResponseMap {
  [key: string]: FacetResponse[];
}

export interface SearchResponse {
  snippets: Result[];
  facets?: FacetResponseMap;
  hits: number;
  iiif_flex_url?: string;
  selection?: {
    query_string?: string[];
    facets?: FacetSelection;
    viewer?: string;
  };
  popup?: PopupType;
  // outdated
  facet_counts?: any;
  no_facet_values?: any;
  // internal runtime fields
  expectsFacetInfo?: boolean;
}
