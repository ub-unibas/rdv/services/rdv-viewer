import {environment} from "../Environment";
import {BackendViewerType, ListType, Page, ViewOrder} from "./settings.model";

export const orderOfViewer = (v: string, forView: Page): number => {
  let n = 50;
  if (environment.documentViewer && environment.documentViewer[v] && environment.documentViewer[v].enabledForPage) {
    environment.documentViewer[v].enabledForPage.forEach((e: ViewOrder) => {
      if (e.view === forView) {
        n = e.order;
      }
    });
  }

  return n;
};

export let possibleViewers = (forView: Page): string[] => {
  const documentViewer = environment.documentViewer;
  return Object.keys(documentViewer)
    .filter(key => documentViewer[key].enabled === true
      && (!documentViewer[key].enabledForPage
        || documentViewer[key].enabledForPage.filter(vd => vd.view === forView).length > 0));
};

export let checkViewersFromBackend = (forView: Page, viewersFromBackend: string[]): string[] => {
  const documentViewer = environment.documentViewer;
  return viewersFromBackend.filter((v) => {
    let viewerUnknown = !documentViewer[v] || documentViewer[v].enabled === false;
    if (!viewerUnknown && documentViewer[v].enabledForPage) {
      viewerUnknown = documentViewer[v].enabledForPage.filter(vo => vo.view === forView).length === 0;
    }
    if (viewerUnknown) {
      console.error("Backend returned unknown viewer (for current view) or viewer is disabled: '" + v + "'. " +
        "Possible configured viewers are: " + possibleViewers(forView).join(", "));
    }
    return !viewerUnknown;
  });
};

export let availableIiifViewers = (forView: Page, viewersFromBackend: BackendViewerType): string[] => {
  let viewers: string[] = [];
  if (environment.documentViewer && viewersFromBackend !== "NONE") {
    if (viewersFromBackend === "ALL") {
      viewers = possibleViewers(forView);
    } else {
      // viewersFromBackend is a string[]
      viewers = viewersFromBackend;
    }
    viewers.sort((v1, v2) => orderOfViewer(v1, forView) - orderOfViewer(v2, forView));
  }
  return viewers;
};

export let defaultViewer = (): string => {
  return environment.defaultDocumentViewerName || ListType.LIST.toString();
};
