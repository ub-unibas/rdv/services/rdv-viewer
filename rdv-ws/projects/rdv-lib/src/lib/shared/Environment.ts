import {SettingsModel} from "./models/settings.model";

export let environment: SettingsModel;

export function initEnv(e: any): void {
  environment = e;
}

