/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {SavedQuery} from '../models/saved-query.model';
import {SavedQueryActions, SavedQueryActionTypes} from '../actions/saved-query.actions';

/**
 * List of saved queries
 */
export interface State extends EntityState<SavedQuery> {
}

/**
 * @ignore
 */
export const adapter: EntityAdapter<SavedQuery> = createEntityAdapter<SavedQuery>();

/**
 * @ignore
 */
export const initialState: State = adapter.getInitialState({});

/**
 * @ignore
 */
export function reducer(
  state = initialState,
  action: SavedQueryActions
): State {
  switch (action.type) {
    case SavedQueryActionTypes.AddSavedQuery: {
      return adapter.addOne(action.payload.savedQuery, state);
    }

    case SavedQueryActionTypes.AddSavedQueries: {
      return adapter.addMany(action.payload.savedQueries, state);
    }

    case SavedQueryActionTypes.DeleteSavedQuery: {
      return adapter.removeOne(action.payload.id, state);
    }

    default: {
      return state;
    }
  }
}

/**
 * @ignore
 */
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
