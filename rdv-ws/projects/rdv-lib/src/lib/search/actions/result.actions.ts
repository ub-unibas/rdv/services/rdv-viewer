/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Action} from '@ngrx/store';
import {Result} from '../models/result.model';
import {PopupType} from "../reducers/result.reducer";

/***
 * @ignore
 */
export enum ResultActionTypes {
  AddResults = '[Result] Add Results',
  ClearResults = '[Result] Clear Results',
  UpdateIIIFUrl = '[Result] Use IIIF url',
  UpdatePopup = '[Result] Show popup',
}

/**
 * Adds elements to result list
 */
export class AddResults implements Action {
  /**
   * @ignore
   */
  readonly type = ResultActionTypes.AddResults;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(public payload: { results: Result[] }) {
  }
}

/**
 * Sets IIIF Universal Viewer's url to display search results.
 */
export class UpdateIIIFUrl implements Action {
  /**
   * @ignore
   */
  readonly type = ResultActionTypes.UpdateIIIFUrl;

  /**
   * Contains manifest url to be displayed
   * @param url
   */
  constructor(public url: string) {
  }
}

/**
 * Sets IIIF Universal Viewer's url to display search results.
 */
export class UpdatePopup implements Action {
  /**
   * @ignore
   */
  readonly type = ResultActionTypes.UpdatePopup;

  /**
   * Contains popup info to be displayed
   * @param popup
   */
  constructor(public popup: PopupType) {
  }
}

export interface CommonSelectedFacetValue {
  label: string;
  value: any;
}

export interface FacetSelection {
  [key: string]: { values: CommonSelectedFacetValue[], operator?: string, valueOrder?: string };
}


/**
 * Removes all elements from result list
 */
export class ClearResults implements Action {
  /**
   * @ignore
   */
  readonly type = ResultActionTypes.ClearResults;
}


/**
 * @ignore
 */
export type ResultActions =
  AddResults
  | ClearResults
  | UpdateIIIFUrl
  | UpdatePopup;
