/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewChild} from "@angular/core";
import * as fromFormActions from "../actions/form.actions";
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers/index";
import * as fromQueryActions from "../actions/query.actions";
import {environment} from "../../shared/Environment";
import {FacetFieldsModel} from "../../shared/models/settings.model";
import {InFacetSearchComponent} from "./in-facet-search.component";

/**
 * Displays simple facet.
 */
@Component({
  selector: 'app-facet',
  template: `
    <ng-template #facetValueTemplate let-key="key" let-value="value" let-selected="selected">
      <button
        (click)="facetAction(key, value, selected)"
        type="button"
        class="facet__entry link-black {{selected ? 'facet__entry--selected' : ''}}">
        <span class="facet__name">{{value.label}}</span>
        <span class="facet__count"> ({{value.count}})</span>
      </button>
    </ng-template>
    <div class="facet">
      <app-operator-selector
        [key]="key"
        (changed)="searchOnChange()"
        [facetValueSelector]="fromSearch.getFacetValuesByKey"></app-operator-selector>
      <app-facet-order-selector
        [key]="key"
        (changed)="searchOnChange()"
        [facetValueSelector]="fromSearch.getFacetValuesByKey"></app-facet-order-selector>
      <ul class="facet__list" *ngIf="(facetFieldByKey$ | async)(key).values?.length > 0">
        <li *ngFor="let value of (facetFieldByKey$ | async)(key).values">
          <button
            (click)="facetAction(key, value, true)"
            type="button"
            class="facet__entry link-black facet__entry--selected">
            <span class="facet__name">{{value.label}}</span>
          </button>
        </li>
      </ul>
      <app-in-facet-search #inFacetSearch
                           *ngIf="facetFieldsConfig[key].searchWithin"
                           [key]="key"
                           (changed)="updateFacets($event)"
                           [defaultSearch]="(inFacetSearch$ |async)[facetFieldsConfig[key].field]"
      ></app-in-facet-search>
      <ul class="facet__list">
        <li *ngIf="loading" class="rotating"></li>
        <app-expandable
          [itemTemplate]="facetValueTemplate"
          [itemsPerExpansion]="facetFieldsConfig[key].expandAmount"
          [items]="buildTemplateItems(facetFieldByKey$ | async, facetFieldCountByKey$ | async)"></app-expandable>
      </ul>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleFacetComponent implements OnDestroy, OnChanges {
  @Input() key: string;
  @Input() visible: boolean;
  @ViewChild("inFacetSearch") inFacetSearchRef: InFacetSearchComponent;

  facetFieldByKey$: Observable<any>;
  facetFieldCountByKey$: Observable<any>;
  inFacetSearch$: Observable<any>;

  facetFieldsConfig: FacetFieldsModel;
  protected timer;
  loading: boolean;

  constructor(private _searchStore: Store<fromSearch.State>) {
    this.facetFieldByKey$ = _searchStore.pipe(select(fromSearch.getFacetValuesByKey));
    this.facetFieldCountByKey$ = _searchStore.pipe(select(fromSearch.getFacetFieldCountByKey));
    this.inFacetSearch$ = _searchStore.pipe(select(fromSearch.getAllInFacetSearchValues));
    this.facetFieldsConfig = environment.facetFields;
  }

  facetAction(key, value, selected: boolean) {
    if (selected) {
      this._searchStore.dispatch(new fromFormActions.RemoveFacetValue({facet: key, id: value.id}));
      this._searchStore.dispatch(new fromFormActions.ClearAllInFacetSearch());
    } else {
      this._searchStore.dispatch(new fromFormActions.AddFacetValue(
        {facet: key, id: value.id, label: value.label, value: value.value}));
      this._searchStore.dispatch(new fromFormActions.ClearAllInFacetSearch(this.facetFieldsConfig[key].field));
    }
    this.doSearch();
  }

  searchOnChange() {
    this._searchStore.dispatch(new fromFormActions.ClearAllInFacetSearch());
    this.doSearch();
  }

  doSearch() {
    this._searchStore.dispatch(new fromQueryActions.SetOffset(0));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  buildTemplateItems(facetFieldByKey, facetFieldCountByKey) {
    this.loading = false;
    const facet = facetFieldByKey(this.key);
    let values = facetFieldCountByKey(facet.field);
    if (!values) {
      values = [];
    }
    return values.filter((v) => !this.isSelected(facet, v.id)).map((v) => ({key: this.key, value: v, selected: false}));
  }

  protected isSelected(facet, id): boolean {
    const selected = facet.values.filter((f) => f.id === id);
    return selected.length !== 0;
  }

  updateFacets(event) {
    const facetField = environment.facetFields[this.key].field;
    const prefix = event.prefix;
    this._searchStore.dispatch(new fromFormActions.SetInFacetSearch(facetField, prefix));
    this.clearTimer();
    const handler = () => this._searchStore.dispatch(new fromQueryActions.InFacetSearch());
    if (event.immediate) {
      handler();
    } else {
      this.loading = true;
      this.timer = setTimeout(handler, 500);
    }
  }

  protected clearTimer() {
    this.loading = false;
    if (this.timer) {
      clearTimeout(this.timer);
      delete this.timer;
    }
  }

  ngOnDestroy(): void {
    this.clearTimer();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.visible && !changes.visible.firstChange) {
      this.clearTimer();
    }
  }

  get fromSearch() {
    return fromSearch;
  }

  clearInFacetSearch() {
    if (this.inFacetSearchRef) {
      this.inFacetSearchRef.clearSearchField();
    }
  }
}
