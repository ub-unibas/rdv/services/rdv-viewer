/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Store} from '@ngrx/store';

import {environment} from '../../shared/Environment';
import * as fromSearch from "../reducers/index";
import * as fromLayoutActions from "../actions/layout.actions";
import {FacetFieldType} from "../../shared/models/settings.model";

/**
 * Viewport for facets and ranges
 */
@Component({
  selector: 'app-visual-search',
  template: `
    <!-- DIV-Container fuer Tab-Links -->
    <nav class="nav nav-pills"
         id="facet-pills-tab"
         role="tablist">

      <!-- Tab-Ueberschriften fuer Facetten als Pills-Link -->
      <a *ngFor="let key of simpleFilter(facetFieldsConfig | objectKeys)"
         class="nav-link text-sm-center px-2 py-0"
         [ngClass]="'order-' + facetFieldsConfig[key].order"
         [class.active]="facetFieldsConfig[key].order == 1"
         [id]="'facet-pills-'+key+'-tab'"
         data-toggle="pill"
         href="#"
         (click)="changeView('facet-pills-'+key)"
         role="tab"
         aria-controls="'pills-'+key"
         aria-expanded="true">{{facetFieldsConfig[key].label | translate}}</a>

      <!-- Tab-Ueberschriften fuer Ranges als Pills-Link -->
      <a *ngFor="let key of rangeFieldsConfig | objectKeys"
         class="nav-link text-sm-center px-2 py-0"
         [ngClass]="'order-' + rangeFieldsConfig[key].order"
         [class.active]="rangeFieldsConfig[key].order == 1"
         id="facet-pills-{{key}}-tab"
         data-toggle="pill"
         href="#"
         (click)="changeView('facet-pills-'+key)"
         role="tab"
         aria-controls="'facet-pills-'+key"
         aria-expanded="true">{{rangeFieldsConfig[key].label | translate}}</a>
    </nav>

    <!-- DIV-Container fuer Tab-Inhalte -->
    <div style="height: 300px; overflow-y: scroll;"
         class="tab-content mt-2"
         id="facet-pills-tabContent">
      <app-facets></app-facets>
      <app-ranges></app-ranges>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VisualSearchComponent {
  rangeFieldsConfig: any;
  facetFieldsConfig: any;

  constructor(private _searchStore: Store<fromSearch.State>) {
    this.rangeFieldsConfig = environment.rangeFields;
    this.facetFieldsConfig = environment.facetFields;
  }

  changeView(view: string) {
    this._searchStore.dispatch(new fromLayoutActions.ShowFacetOrRange(view));
    return false;
  }

  simpleFilter(facetConfigKeys) {
    return facetConfigKeys.filter((k) => this.facetFieldsConfig[k].facetType === FacetFieldType.SIMPLE
      || this.facetFieldsConfig[k].facetType === FacetFieldType.SUBCATEGORY);
  }
}
