/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';

import * as fromSearch from '../reducers/index';
import * as fromFormActions from '../actions/form.actions';

/**
 * Root component for all components managing (saved) searches and baskets
 */
@Component({
  selector: 'app-manage-search',
  template: `
      <div class="query-overview bg-info text-white p-2 rounded">

          <button class="btn btn-default btn-sm"
                  (click)="resetSearch()">Neue Suche
          </button>

          <hr>

          <!-- Uebersicht der aktuellen Suchparameter, Link der Suche, Suche speichern -->
          <div class="no-gutters">

              <app-params-set></app-params-set>

              <div class="d-flex no-gutters mh-lh">

                  <label>Suche als Link:</label>
                  <div class="col">
                      Link kopieren
                      <app-copy-link [data]="query$ | async" [small]="true"></app-copy-link>
                  </div>
              </div>

              <app-save-query></app-save-query>

          </div>

          <app-manage-saved-queries></app-manage-saved-queries>

          <hr>

          <app-basket-list></app-basket-list>

      </div>
  `,
  styles: [`
      .mh-lh {
          line-height: 30px
      }

      .query-overview label {
          width: 140px;
      }

      label {
          margin-bottom: 0;
      }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ManageSearchComponent {
  query$: Observable<any>;

  constructor(private _searchStore: Store<fromSearch.State>) {
    this.query$ = _searchStore.pipe(select(fromSearch.getFormValues));
  }

  resetSearch() {
    this._searchStore.dispatch(new fromFormActions.ResetAll());
  }
}
