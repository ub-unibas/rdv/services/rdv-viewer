/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, Output} from "@angular/core";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers/index";
import {combineLatest, Observable} from "rxjs";
import {environment} from "../../shared/Environment";
import {FacetFieldType, HistogramFieldModel, HistogramFieldType} from "../../shared/models/settings.model";
import {map, take} from "rxjs/operators";
import * as fromFormActions from "../actions/form.actions";
import * as fromQueryActions from "../actions/query.actions";

interface Group {
  pos?: string;
  members: any[];
}

/**
 * Displays collapsible facets.
 */
@Component({
  selector: 'app-collapsible-facets',
  template: `
    <div class="{{facetClasses(showFacets)}}">
      <div class="collapsible-facets__container-overlay"></div>
      <div class="collapsible-facets__container-elements">
        <div class="d-block d-sm-none">
          <button class="btn collapsible-facets__use-filter link-black" (click)="closeFacet()">
            {{'collapsible-facets.use_filter' | translate}}
          </button>
        </div>
        <div *ngIf="showFacets"
             class="collapsible-facets__available"
             [class.collapsible-facets__available--toggable]="forceToggled"
        >
          <div class="pt-sm-2">
            <h4 class="search-results__title d-sm-none">{{'search-results.title' | translate:{value: (searchCount$ | async)} }}</h4>
          </div>
          <ng-container *ngFor="let key of
              filterCountKeys(selectedFacetValueByKey$ | async, facetFieldCountByKey$ | async); last as isLast;">
            <div [class.active]="facetFieldsConfig[key].order == 1"
                 [class.last]="isLast"
                 class="collapsible-facets"
                 role="listitem">
              <ng-container [ngSwitch]="facetFieldsConfig[key].facetType">
                <app-collapsible-facet *ngSwitchCase="simpleType()"
                                       [open]="facetOpen(openFacet$ | async, key)"
                                       [key]="key"
                                       [title]="facetFieldsConfig[key].label | translate"
                                       (openFacetEmitter)="switchFacetOpenState($event)"
                ></app-collapsible-facet>
                <ng-container *ngSwitchCase="subcategoryType()">
                  <app-collapsible [title]="facetFieldsConfig[key].label | translate"
                                   [open]="facetOpen(openFacet$ | async, key)"
                                   [key]="key"
                                   (changed)="openFacetStateChanged({key: key, open: $event})"
                  >
                    <app-subcat-facet [key]="key"></app-subcat-facet>
                  </app-collapsible>
                </ng-container>
                <ng-container *ngSwitchCase="histogramType()">
                  <ng-container [ngSwitch]="facetFieldsConfig[key].data_type">
                    <app-collapsible *ngSwitchCase="histogramIntType()"
                                     [open]="facetOpen(openFacet$ | async, key)"
                                     [title]="facetFieldsConfig[key].label | translate"
                                     [key]="key"
                                     (changed)="openFacetStateChanged({key: key, open: $event})"
                    >
                      <app-int-histogram-facet *ngIf="!facetFieldsConfig[key].showAggs" [key]="key"></app-int-histogram-facet>
                      <app-int-hierarchic-facet *ngIf="facetFieldsConfig[key].showAggs" [key]="key"></app-int-hierarchic-facet>
                    </app-collapsible>
                    <app-collapsible *ngSwitchCase="histogramDateType()"
                                     [open]="facetOpen(openFacet$ | async, key)"
                                     [title]="facetFieldsConfig[key].label | translate"
                                     [key]="key"
                                     (changed)="openFacetStateChanged({key: key, open: $event})"
                    >
                      <app-date-histogram-facet *ngIf="!facetFieldsConfig[key].showAggs" [key]="key"></app-date-histogram-facet>
                      <app-date-hierarchic-facet *ngIf="facetFieldsConfig[key].showAggs" [key]="key"></app-date-hierarchic-facet>
                    </app-collapsible>
                    <app-collapsible *ngSwitchCase="histogramGeoType()"
                                     [open]="facetOpen(openFacet$ | async, key)"
                                     [title]="facetFieldsConfig[key].label | translate"
                                     [key]="key"
                                     (changed)="openFacetStateChanged({key: key, open: $event})"
                    >
                      <app-geo-histogram-facet *ngIf="!facetFieldsConfig[key].showAggs" [key]="key"></app-geo-histogram-facet>
                      <app-geo-hierarchic-facet *ngIf="facetFieldsConfig[key].showAggs" [key]="key"></app-geo-hierarchic-facet>
                    </app-collapsible>
                  </ng-container>
                </ng-container>
                <app-collapsible *ngSwitchCase="hierarchicType()"
                                 [open]="facetOpen(openFacet$ | async, key)"
                                 [title]="facetFieldsConfig[key].label | translate"
                                 [key]="key"
                                 (changed)="openFacetStateChanged({key: key, open: $event})"
                >
                  <app-hierarchic-facet [key]="key"></app-hierarchic-facet>
                </app-collapsible>
              </ng-container>
            </div>
          </ng-container>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollapsibleFacetsComponent implements OnDestroy {
  @Input() forceToggled: boolean;
  @Input() showFacets: boolean;
  @Output() closeFacetEmitter = new EventEmitter<true>();

  facetFieldsConfig: any;

  selectedFacetValueByKey$: Observable<any>;
  facetFieldCountByKey$: Observable<any>;
  inFacetSearch$: Observable<any>;
  openFacet$: Observable<any>;
  searchCount$: Observable<number>;

  constructor(protected searchStore: Store<fromSearch.State>) {
    this.facetFieldsConfig = environment.facetFields;

    this.selectedFacetValueByKey$ = combineLatest([
      searchStore.select(fromSearch.getFacetValues),
      searchStore.select(fromSearch.getHistogramValues),
      searchStore.select(fromSearch.getHierarchyValues),
    ]).pipe(map(([facets, histograms, hierarchies]) => ((key: string) => {
      return hierarchies[key] ?
        hierarchies[key].values : (histograms[key] ?
          histograms[key].values : facets[key].values);
    })));

    this.facetFieldCountByKey$ = combineLatest([
      searchStore.select(fromSearch.getFacetFieldCountByKey),
      searchStore.select(fromSearch.getFacetHistogramCountByKey),
      searchStore.select(fromSearch.getHierarchicFacetCountByKey),
    ]).pipe(map(([facets, histograms, hierarchies]) => ((field: string) => {
      let v = hierarchies(field);
      if (v) {
        return v;
      }
      v = histograms(field);
      return v ? v : facets(field);
    })));

    this.inFacetSearch$ = searchStore.select(fromSearch.getAllInFacetSearchValues);
    this.openFacet$ = searchStore.select(fromSearch.getAllOpenFacet);
    this.searchCount$ = searchStore.pipe(select(fromSearch.getTotalResultsCount));
  }

  filterCountKeys(selectedFacetValueByKey, facetFieldCountByKey) {
    let inFacetSearchFields;
    this.inFacetSearch$.pipe(take(1)).subscribe((v) => inFacetSearchFields = Object.keys(v));
    if (!inFacetSearchFields) {
      inFacetSearchFields = [];
    }
    let openFacets;
    this.openFacet$.pipe(take(1)).subscribe((v) => openFacets = Object.keys(v));
    if (!openFacets) {
      openFacets = [];
    }
    const config = this.facetFieldsConfig;
    // console.log("---- FILTER", inFacetSearchField);
    return Object.keys(config)
      .filter((key) => {
        const fieldConfig = config[key];
        // console.log("FIELD", key, fieldConfig);
        const fieldName = fieldConfig.field;
        // always show int histogram facets
        if (fieldConfig.facetType === FacetFieldType.HISTOGRAM) {
          const histogramFacet = fieldConfig as HistogramFieldModel;
          if (histogramFacet.data_type === HistogramFieldType.INT || histogramFacet.data_type === HistogramFieldType.GEO) {
            // console.log("SHOW HISTO", key);
            return true;
          }
        }
        const values = facetFieldCountByKey(fieldName);
        const selectedFacetValues = selectedFacetValueByKey(key);
        // console.log("VALUES", key, "values=", values, "sel=", selectedFacetValues);
        // show facet if either has selected values or unselected values
        // but keep facet even if empty if an in-facet search was done;
        // show also facet if open and has values
        return (selectedFacetValues && selectedFacetValues.length > 0)
          || (values && values.length > 0)
          || (inFacetSearchFields.indexOf(fieldName) >= 0)
          || (openFacets.indexOf(fieldName) >= 0 && values && values.length > 0);
      })
      .sort((a, b) => config[a].order - config[b].order);
  }

  public simpleType(): string {
    return FacetFieldType.SIMPLE.toString();
  }

  public subcategoryType(): string {
    return FacetFieldType.SUBCATEGORY.toString();
  }

  public histogramType(): string {
    return FacetFieldType.HISTOGRAM.toString();
  }

  public histogramIntType(): string {
    return HistogramFieldType.INT.toString();
  }

  public histogramDateType(): string {
    return HistogramFieldType.DATE.toString();
  }

  public histogramGeoType(): string {
    return HistogramFieldType.GEO.toString();
  }

  public hierarchicType(): string {
    return FacetFieldType.HIERARCHIC.toString();
  }

  facetClasses(show: boolean): string {
    const showClass = "collapsible-facets--show-in-overlay";
    let cssClasses = "collapsible-facets__container";
    if (show) {
      cssClasses += " " + showClass;
      document.documentElement.classList.add(showClass);
      document.body.classList.add(showClass);
    } else {
      document.documentElement.classList.remove(showClass);
      document.body.classList.remove(showClass);
    }
    return cssClasses;
  }

  closeFacet() {
    this.closeFacetEmitter.emit(true);
  }

  facetOpen(openFacet, key: string): boolean {
    return this.facetFieldsConfig[key].field in openFacet;
  }

  switchFacetOpenState({key, open}) {
    this.searchStore.dispatch(new fromFormActions.SetFacetOpenedInUi(
      {field: this.facetFieldsConfig[key].field, opened: open}));
    if (open) {
      // load missing facets
      this.searchStore.dispatch(new fromQueryActions.SimpleSearch());
    }
  }

  openFacetStateChanged(facetInfo) {
    this.switchFacetOpenState(facetInfo);
    if (!facetInfo.open) {
      // undefined == remove, because empty search "" is possible too!
      this.searchStore.dispatch(new fromFormActions.SetInFacetSearch(this.facetFieldsConfig[facetInfo.key].field, undefined));
    }
  }

  ngOnDestroy(): void {
    this.facetClasses(false);
  }
}
