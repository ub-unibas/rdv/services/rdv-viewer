/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import tinymce from 'tinymce';
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {TranslateService} from "@ngx-translate/core";
import {environment} from "../../shared/Environment";

/**
 * Displays a row in the result or  list
 */
@Component({
  selector: 'app-tinymce',
  template: `
    <div class="tinymce">
      <div #tinymce [innerHTML]="html"></div>
      <div *ngIf="loading$ | async">{{'richtext-editor.loading' | translate}}</div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TinymceComponent implements AfterViewInit {
  @Input() html: string;
  @ViewChild("tinymce") tinymceRef: ElementRef<HTMLElement>;

  @Output() changedHtml = new EventEmitter<string>();
  loadingSubject: Subject<boolean>;
  loading$: Observable<boolean>;

  constructor(protected translate: TranslateService) {
    this.loadingSubject = new BehaviorSubject<boolean>(true);
    this.loading$ = this.loadingSubject.asObservable();
  }

  ngAfterViewInit(): void {
    tinymce.baseURL = "/assets/tinymce/";
    const editorSettings = environment.richtextEditor || {};
    tinymce.init({
      ...editorSettings,
      target: this.tinymceRef.nativeElement,
      setup: (editor) => {
        editor.on('Change', (content) => {
          const currentText = content.level.content;
          this.changedHtml.emit(currentText);
        });
        editor.on('init', () => this.loadingSubject.next(false));
      },
      language: this.translate.currentLang,
    });
  }

}
