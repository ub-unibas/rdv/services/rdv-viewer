/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {debounceTime, distinctUntilChanged, filter, map, take} from 'rxjs/operators';
import {ChangeDetectionStrategy, Component, HostListener, OnDestroy, OnInit} from '@angular/core';

import {ActivatedRoute} from '@angular/router';
import {select, Store} from '@ngrx/store';

import * as fromRoot from '../../reducers/index';
import * as fromSearch from '../reducers/index';
import * as fromQueryActions from '../actions/query.actions';
import * as fromBasketActions from '../actions/basket.actions';
import * as fromBasketResultActions from '../actions/basket-result.actions'
import * as fromSavedQueryActions from '../actions/saved-query.actions';
import {environment} from '../../shared/Environment';
import {randomHashCode} from '../../shared/utils';
import {combineLatest} from "rxjs";
import {Subscription} from "rxjs";
import {Observable} from "rxjs";
import {UrlParamsService} from "../../shared/services/url-params.service";
import {PopupType} from "../reducers/result.reducer";
import {TranslateService} from "@ngx-translate/core";
import {QueryParams} from "../../shared/models/query-format";
import * as LZString from 'lz-string';

/**
 * Root component for {@link SearchModule}. Responsible for initial setup of search state and for watching changes in search form
 */
@Component({
  selector: 'app-search',
  template: `
    <div class="container mt-2">
      <app-manage-search></app-manage-search>
      <app-search-params></app-search-params>
      <app-result-lists></app-result-lists>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent implements OnInit, OnDestroy {
  /**
   * All saved baskets
   */
  private _baskets: any;
  /**
   * All saved search queries
   */
  private _savedQueries: any;
  /**
   * Latest version of search query
   */
  protected combinedQuery: any;

  /**
   * Subscription to query change observer.
   */
  protected watchSubscription: Subscription;

  protected watch$: Observable<any>;

  protected popup$: Observable<PopupType>;

  /**
   * Decodes and parses search parameters from URL as JSON
   * @param {Object} params
   * @private
   */
  private static _loadQueryFromUrl(params: any) {
    return JSON.parse(LZString.decompressFromEncodedURIComponent(params.get("search")));
  }

  /**
   * Parses saved search queries from localstorage as JSON
   *
   * @private
   */
  private static _loadQueryFromLocalStorage() {
    return JSON.parse(localStorage.getItem("userQuery"));
  }

  /**
   * Watches for search form changes and updates search state accordingly
   *
   * @param {ActivatedRoute} route Activated route
   * @param {Store<>} rootStore NgRx application root store
   * @param {Store<>} searchStore NgRx root search form store
   * @param translate
   * @param urlService
   */
  constructor(protected route: ActivatedRoute,
              protected rootStore: Store<fromRoot.State>,
              protected searchStore: Store<fromSearch.State>,
              protected translate: TranslateService,
              protected urlService: UrlParamsService) {

    searchStore.pipe(select(fromSearch.getCurrentBasket),
      filter(x => !!x))
      .subscribe(basket => {
        this.searchStore.dispatch(new fromBasketResultActions.ClearBasketResults());
        // TODO basket is currently not used; perhaps wrong request, because response returns 500
        // this.searchStore.dispatch(new fromQueryActions.MakeBasketSearchRequest(basket));
      });
    searchStore.pipe(select(fromSearch.getAllBaskets)).subscribe(baskets => this._baskets = baskets);
    searchStore.pipe(select(fromSearch.getAllSavedQueries)).subscribe(savedQueries => this._savedQueries = savedQueries);

    this.popup$ = searchStore.pipe(select(fromSearch.getPopup));
  }

  /**
   * Saves data to localstorage before leaving app
   */
  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHandler() {
    this._writeToLocalStorage();
  }

  /**
   * Loads saved searches and baskets from URL or localstorage
   */
  ngOnInit() {
    if (localStorage.getItem("savedBaskets") && JSON.parse(localStorage.getItem("savedBaskets")).length) {
      this._loadBasketFromLocalStorage();
    } else {
      this._generateInitialBasket();
    }

    this.route.queryParamMap.subscribe(params => {

      // if (params.get("search")) {
      //   this._searchRequestFromUrl(params);
      // } else {
      //   this.handleLocalStorageQuery();
      // }

      if (params.get("basket")) {
        this._loadBasketFromUrl(params);
      }
    });

    this._loadSavedSearchQueries();

    this._watchSearchRequestChanges();
  }

  protected handleLocalStorageQuery() {
    let popup;
    this.popup$.pipe(take(1)).subscribe((p) => popup = p);
    // popup query overwrites locally stored query ...
    if (!popup && (localStorage.getItem("userQuery") || localStorage.getItem("userQuery") !== "undefined")) {
      this._searchRequestFromLocalStorage();
    }
  }

  /**
   * Writes to localstorage when leaving component
   */
  ngOnDestroy(): void {
    this._writeToLocalStorage();
    if (this.watchSubscription) {
      this.watchSubscription.unsubscribe();
    }
  }

  /**
   * Watches for changes in fields required for search requests and emits such a request whenever a field has been changed
   *
   * @protected
   */
  protected _watchSearchRequestChanges() {
    this.watch$ = this.createWatcher();
    this.watchSubscription = this.watch$
      .subscribe(vals => {
        this.combinedQuery = vals;
        this.searchStore.dispatch(new fromQueryActions.MakeSearchRequest(vals));
      });
  }

  /**
   * Build query change observer.
   *
   * @returns {Observable<any>}
   */
  protected createWatcher(): Observable<any> {
    return combineLatest([
      this.searchStore.pipe(
        select(fromSearch.getQueryParams),
        map(val => {
            return {
              queryParams: {
                rows: val.rows,
                sortDir: val.sortOrder,
                sortField: val.sortField,
                start: val.offset
              },
              lang: this.translate.currentLang
            };
          }
        ),
      ),
      this.searchStore.pipe(
        select(fromSearch.getCombinedSearchQueries),
        debounceTime(750),
        distinctUntilChanged(),
      )
    ]).pipe(
      map(val => Object.assign(val[0], val[1])));
  }

  /**
   * Issues search request based on encoded search parameters in URL
   *
   * @param {Object} params Encoded search parameters
   * @private
   */
  private _searchRequestFromUrl(params: any) {
    this.searchStore.dispatch(new fromQueryActions.MakeSearchRequest(SearchComponent._loadQueryFromUrl(params)));
  }

  /**
   * Issues search request based on saved search parameters in localstorage
   *
   * @private
   */
  private _searchRequestFromLocalStorage() {
    this.searchStore.dispatch(new fromQueryActions.MakeSearchRequest(SearchComponent._loadQueryFromLocalStorage()));
  }

  /**
   * Decodes basket information from url and loads the basket into state store
   *
   * @param {Object} params Encoded basket information
   * @private
   */
  private _loadBasketFromUrl(params: any) {
    const compressedBasket = params.get("basket");
    const basketFromLink = JSON.parse(LZString.decompressFromEncodedURIComponent(compressedBasket));
    const hash = randomHashCode();
    this.searchStore.dispatch(new fromBasketActions.ClearBaskets());
    this.searchStore.dispatch(new fromBasketActions.AddBasket({basket: {...basketFromLink, id: hash}}));

    this.searchStore.dispatch(new fromBasketActions.SelectBasket({id: hash}));
  }

  /**
   * Loads saved search queries from localstorage
   *
   * @private
   */
  private _loadSavedSearchQueries() {
    const localStorageSavedUserQueries = localStorage.getItem("savedUserQueries");
    if (localStorageSavedUserQueries) {
      this.searchStore.dispatch(new fromSavedQueryActions.AddSavedQueries({savedQueries: JSON.parse(localStorageSavedUserQueries)}));
    }
  }

  /**
   * Loads basket from localstorage
   * @private
   */
  private _loadBasketFromLocalStorage() {
    const parsedBaskets = JSON.parse(localStorage.getItem("savedBaskets")).map((x: any) => {
      return {...x, id: randomHashCode()}
    });
    this.searchStore.dispatch(new fromBasketActions.ClearBaskets());
    this.searchStore.dispatch(new fromBasketActions.AddBaskets({baskets: parsedBaskets}));
    this.searchStore.dispatch(new fromBasketActions.SelectBasket({id: parsedBaskets[0].id}));
  }

  /**
   * Builds initial basket
   *
   * @private
   */
  private _generateInitialBasket() {
    const hash = randomHashCode();
    this.searchStore.dispatch(new fromBasketActions.AddBasket({
      basket: {
        id: hash,
        name: 'Meine Merkliste 1',
        ids: [],
        queryParams: {
          rows: environment.queryParams.rows,
          start: environment.queryParams.offset,
          sortField: environment.queryParams.sortField,
          sortDir: environment.queryParams.sortOrder,
        } as QueryParams,
        lang: this.translate.currentLang
      }
    }));
    this.searchStore.dispatch(new fromBasketActions.SelectBasket({id: hash}));
  }

  /**
   * Writes current query parameters, saved search queries and saved baskets to localstorage
   *
   * @private
   */
  private _writeToLocalStorage() {
    localStorage.setItem("userQuery", JSON.stringify(this.combinedQuery));
    localStorage.setItem("savedUserQueries", JSON.stringify(this._savedQueries));
    localStorage.setItem("savedBaskets", JSON.stringify(this._baskets));
  }
}
