/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from "@angular/core";
import {ValidatorResult} from "./abstract-histogram-facet-component.directive";
import {AbstractMinMaxInputComponentDirective} from "./abstract-min-max-input-component.directive";
import {UrlParamsService} from "../../shared/services/url-params.service";

/**
 * Displays two input fields - for a from and to int value.
 */
@Component({
  selector: 'app-min-max-date-input',
  template: `
    <form (ngSubmit)="f.form.valid && onSubmit(from.value, to.value)" #f="ngForm" autocomplete="off" [formGroup]="formGroup">
      <div class="facet_range__from mr-2">
        <label [for]="key + '-from'" class="facet_range__from-label">{{'min-max-date-input.from' | translate}} </label>
        <input [id]="key + '-from'" #from type="date" [min]="min" [max]="max" (change)="checkSubmit(from.value, to.value)">
      </div>
      <div class="facet_range__to mr-2">
        <label [for]="key + '-to'" class="facet_range__to-label">{{'min-max-date-input.to' | translate}} </label>
        <input [id]="key + '-to'" #to type="date" [min]="min" [max]="max" (change)="checkSubmit(from.value, to.value)">
      </div>
      <button
        type="submit"
        class="btn btn-red"
        [disabled]="submitIsDisabled">
        {{'min-max-date-input.select' | translate}}
      </button>
      <ng-container *ngIf="fromGreaterThanTo">
        <div class="error">{{'min-max-date-input.error_from_greater_to' | translate}}</div>
      </ng-container>
    </form>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MinMaxDateInputComponent extends AbstractMinMaxInputComponentDirective<string> {

  constructor(protected urlService: UrlParamsService) {
    super();
  }

  /**
   * @Override
   * @param {string} fromStr
   * @param {string} toStr
   */
  onSubmit(fromStr: string, toStr: string) {
    this.fromGreaterThanTo = false;
    const validated = this.validate(fromStr, toStr);

    if (validated.error) {
      if (validated.error === 'fromGreaterThanTo') {
        this.fromGreaterThanTo = true;
      }
      return;
    }

    this.submitted.emit(validated.ok);
  }

  validate(fromStr: string, toStr: string): ValidatorResult {
    return this.urlService.dateRangeValidator(fromStr, toStr);
  }

}
