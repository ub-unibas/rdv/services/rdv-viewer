/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {SearchComponent} from "./search.component";
import * as fromSearch from "../reducers/index";
import {select, Store} from "@ngrx/store";
import * as fromRoot from "../../reducers/index";
import {ActivatedRoute, Router} from "@angular/router";
import {animate, state, style, transition, trigger} from '@angular/animations';
import {take} from "rxjs/operators";
import {Observable, Subscription} from "rxjs";
import * as fromQueryActions from "../actions/query.actions";
import * as fromLayoutActions from "../actions/layout.actions";
import * as fromFormActions from "../actions/form.actions";
import {buildId} from "../reducers/facet.reducer";
import {BackendSearchService} from "../../shared/services/backend-search.service";
import {
  FacetValueProcessor,
  ReplaceUrlParams,
  SearchActions,
  SearchValueProcessor, ViewerValueProcessor,
  UrlParamNames,
  UrlParamsService
} from "../../shared/services/url-params.service";
import {environment} from "../../shared/Environment";
import {SortOrder} from "../../shared/models/settings.model";
import {PreviousRouteServiceProvider} from "../../shared/services/previous-route-service-provider.service";
import * as fromResultActions from "../actions/result.actions";
import {TranslateService} from "@ngx-translate/core";
import {searchParamObserver} from "../../shared/models/observed-util";
import {SettingsModel} from "../../shared/models/settings.model";
import {Location, LocationStrategy, PathLocationStrategy} from "@angular/common";
import {LOCALIZED_ROUTER_PREFIX} from "./popup-landing.component";
import {SearchField} from "../reducers/form.reducer";

/**
 * Root component for {@link SearchModule}. Responsible for initial setup of search state and for watching changes in search form
 */
@Component({
  selector: 'app-new-search',
  template: `
    <div>
      <app-top></app-top>
      <app-simple-search *ngIf="env.showSimpleSearch !== false"></app-simple-search>
      <!-- Hides currently not implemented map and timeline feature
      <div class="search-toolbar-container">
        <div class="search-toolbar">
          <button type="button" class="btn btn-outline-primary mr-2" (click)="toggleMapDisplay()" i18n="simple-search.toolbar.map">
            {{'new-search.toolbar.map' | translate}}
          </button>
          <button type="button" class="btn btn-outline-primary" (click)="toggleTimelineDisplay()"
                  i18n="simple-search.toolbar.timeline">
            {{'new-search.toolbar.timeline' | translate}}
          </button>
        </div>
        <div [@openCloseMap]="(displayMap$ | async)? 'open' : 'closed'" class="map">
          <div *ngIf="(displayMap$ | async)">
            TODO Map
          </div>
        </div>
        <div [@openCloseTimeline]="(displayTimeline$ | async) ? 'open' : 'closed'" class="timeline">
          <div *ngIf="(displayTimeline$ | async)">
            TODO Timeline
          </div>
        </div>
      </div> -->
      <app-search-results></app-search-results>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  animations: [
    trigger('openCloseMap', [
      state('open', style({
        height: '400px',
        opacity: 1,
      })),
      state('closed', style({
        height: '0px',
        opacity: 0,
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('1s')
      ]),
    ]),
    trigger('openCloseTimeline', [
      state('open', style({
        height: '300px',
        opacity: 1,
      })),
      state('closed', style({
        height: '0px',
        opacity: 0,
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('1s')
      ]),
    ]),
  ],
})
export class NewSearchComponent extends SearchComponent
  implements OnInit, FacetValueProcessor, SearchValueProcessor, ViewerValueProcessor, OnDestroy {
  displayMap$: Observable<any>;
  displayTimeline$: Observable<any>;
  languageSubscription: Subscription;

  constructor(protected route: ActivatedRoute,
              protected rootStore: Store<fromRoot.State>,
              protected searchStore: Store<fromSearch.State>,
              protected backendSearchService: BackendSearchService,
              protected urlService: UrlParamsService,
              protected routeEventsService: PreviousRouteServiceProvider,
              protected translate: TranslateService,
              protected location: Location,
              protected router: Router) {
    super(route, rootStore, searchStore, translate, urlService);
    this.displayMap$ = searchStore.pipe(select(fromSearch.getDisplayMap));
    this.displayTimeline$ = searchStore.pipe(select(fromSearch.getDisplayTimeline));
    const prevUrl = routeEventsService.previousRoutePath.getValue();
    // if previous view was old search view, do a reset! otherwise keep state!
    // especially when coming back from detail view
    if (prevUrl === '/de/old-search') {
      this.searchStore.dispatch(new fromQueryActions.Reset());
    }
    this.languageSubscription = this.translate.onLangChange
      .subscribe((l) => {
        this.searchStore.dispatch(new fromQueryActions.SimpleSearch());
      });
  }

  protected createWatcher(): Observable<any> {
    return searchParamObserver(this.searchStore);
  }

  protected _watchSearchRequestChanges(): void {
    this.watch$ = this.createWatcher();
    this.watchSubscription = this.watch$
      .subscribe(async (vals) => {
        this.combinedQuery = vals;
        if (this.urlService && this.router) {
          let currentQueryParams;
          this.route.queryParams.pipe(take(1)).subscribe(qp => {
            currentQueryParams = qp;
          });
          // create url params from internal state
          const urlParams = new ReplaceUrlParams(this.searchStore, this.backendSearchService);
          // keep current path, but update query params
          const newQueryParams = urlParams.toUrlParams() || {};
          Object.keys(currentQueryParams).map((k) => {
            if (currentQueryParams[k] && !newQueryParams[k]) {
              newQueryParams[k] = null;
            }
          });
          await this.router.navigate([], {
            relativeTo: this.route,
            queryParams: newQueryParams,
            queryParamsHandling: 'merge'
          });
          // fix popup url ...
          if (this.location.path().indexOf('/' + LOCALIZED_ROUTER_PREFIX) >= 0) {
            const cleanUrl = this.location.path().replace("/" + LOCALIZED_ROUTER_PREFIX, "");
            this.location.replaceState(cleanUrl);
          }
        }
      });
  }

  toggleMapDisplay() {
    this.searchStore.dispatch(new fromLayoutActions.ToggleMapDisplay())
  }

  toggleTimelineDisplay() {
    this.searchStore.dispatch(new fromLayoutActions.ToggleTimelineDisplay())
  }

  ngOnInit() {
    this.handleRequestParams();
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    if (this.languageSubscription) {
      this.languageSubscription.unsubscribe();
    }
  }

  protected handleRequestParams() {
    let actions: SearchActions[] = [];

    const subscription = this.route.queryParamMap.subscribe(params => {
      const facetsParams = params.getAll(UrlParamNames.FACETS.toString());
      for (const facetsParam of facetsParams) {
        actions = actions.concat(this.urlService.parseFacet(facetsParam, this));
      }

      const opsParams = params.getAll(UrlParamNames.OPS.toString());
      for (const opsParam of opsParams) {
        actions = actions.concat(this.urlService.parseOperator(opsParam, this));
      }

      const facetValueOrderParams = params.getAll(UrlParamNames.FACET_VALUE_ORDER.toString());
      for (const facetValueOrderParam of facetValueOrderParams) {
        actions = actions.concat(this.urlService.parseFacetValueOrder(facetValueOrderParam, this));
      }

      const searchParam = params.get(UrlParamNames.SEARCHED_TEXT.toString());
      if (searchParam) {
        const searchFields$ = this.searchStore.pipe(select(fromSearch.getSearchValues));
        let defaultSearchFields: { [key: string]: SearchField } = {};
        searchFields$.pipe(take(1)).subscribe((a) => defaultSearchFields = a);
        const searchField = params.get(UrlParamNames.SEARCHED_FIELD.toString());
        actions = actions.concat(this.urlService.parseSearch(searchParam, searchField, defaultSearchFields, this));
      }

      const sortParam = params.get(UrlParamNames.SORT_FIELD.toString());
      if (sortParam) {
        const dirParam = params.get(UrlParamNames.SORT_DIR.toString());
        actions = actions.concat(this.urlService.parseSorting(sortParam, dirParam, environment.sortFields, this));
      }

      const viewerParam = params.get(UrlParamNames.VIEWER.toString());
      if (viewerParam) {
        actions = actions.concat(this.urlService.parseViewer(viewerParam, this));
      }
    });
    subscription.unsubscribe();

    if (actions.length > 0) {
      this.searchStore.dispatch(new fromFormActions.ResetAll());
      actions.map((a) => this.searchStore.dispatch(a));
    }

    let justReceivedPopup = false;
    this.searchStore.pipe(select(fromSearch.getPopup)).pipe(take(1)).subscribe((newPopup) => {
      if (newPopup) {
        justReceivedPopup = true;
        this.searchStore.dispatch(new fromResultActions.UpdatePopup(undefined));
      }
    });

    const prevUrl = this.routeEventsService.previousRoutePath.getValue();
    const fromDetailView = prevUrl.indexOf("/detail/") >= 0;

    // if not coming from popup landing or detail view, do an empty search
    if (!justReceivedPopup && !fromDetailView) {
      let payload;
      if (actions.length > 0) {
        payload = {return_selection: true};
      }
      this.searchStore.dispatch(new fromQueryActions.SimpleSearch(payload));
    }
  }

  // deep url search param callbacks ....

  processSimpleFacet(field: string, label: string, value: any): SearchActions[] {
    return [new fromFormActions.AddFacetValue(
      {facet: field, id: buildId(value), label: label, value: value})];
  }

  processHistogramFacet(field: string, label: string, value: any): SearchActions[] {
    return [new fromFormActions.AddHistogramBoundaries(
      {key: field, id: buildId(value), label: label, value: value})];
  }

  processHierarchicFacet(field: string, label: string, value: any): SearchActions[] {
    return [new fromFormActions.AddHierarchicFacetValue(
      {key: field, id: buildId(value), label: label, value: value})];
  }

  processSimpleSearchValue(searchText: string[], searchField: string): SearchActions[] {
    return [new fromFormActions.UpdateSearchFieldValue({field: searchField, value: searchText})];
  }

  processSorting(sortField: string, sortDir: SortOrder): SearchActions[] {
    return [new fromQueryActions.SetSortOrder(sortDir), new fromQueryActions.SetSortField(sortField)];
  }

  processFacetOperator(field: string, operator: string): SearchActions[] {
    return [new fromFormActions.UpdateFacetOperator({facet: field, value: operator})];
  }

  processFacetValueOrder(field: string, order: string): SearchActions[] {
    return [new fromFormActions.UpdateFacetValueOrder({facet: field, value: order})];
  }

  processViewer(name: string): SearchActions[] {
    return [new fromQueryActions.SetResultListDisplayMode(name)];
  }

  get env(): SettingsModel {
    return environment;
  }
}
