#!/bin/sh

# this script is called in rdv-ws/

mode=$1

dstDir=dist/rdv-lib2

# ---- copy ts files ---------------------------------------------------------------
# this is only needed for custom html tag completion in Intellij
# a better alternative would be to let ng-packagr generate *.metadata.json files
# but this expects a --prod build of this lib, which is currently not working,
# because init functions of some dependencies can't be inlined (non public
# access)

# the two lines below for `ng serve` usage in dependent projects only.
# call this script with `prod` in order to run `ng build --prod` calls.
if [ "$mode" = "dev" ]; then
  echo "DEV MODE -- copying source files"
  cp -r projects/rdv-lib/src/lib/* $dstDir/lib/
  cp projects/rdv-lib/src/public-api.ts $dstDir/
fi

# ---- copy assets -----------------------------------------------------------------
# ng-packagr doesn't support globs

dst=$dstDir/assets

mkdir -p $dst

cp -r projects/rdv-lib/src/assets/* $dst/

cp -r node_modules/universalviewer/dist/* $dst/uv/

mkdir -p $dst/tinymce/themes
cp -r node_modules/tinymce/themes/* $dst/tinymce/themes/

mkdir -p $dst/tinymce/plugins
cp -r node_modules/tinymce/plugins/* $dst/tinymce/plugins/

mkdir -p $dst/tinymce/skins/
cp -r node_modules/tinymce/skins/* $dst/tinymce/skins/

mkdir -p $dst/tinymce/icons/
cp -r node_modules/tinymce/icons/* $dst/tinymce/icons/

mkdir -p $dst/tinymce/langs/
cp -r node_modules/tinymce-i18n/langs5/* $dst/tinymce/langs/

cp -r dist/mirador-with-plugins/* $dst/mirador/

# ---- copy css/scss ---------------------------------------------------------------

mkdir -p $dstDir/scss/
cp -r projects/rdv-lib/src/scss/* $dstDir/scss/

# ---- switch old and new code as fast as possible ---------------------------------

if test -e "dist/rdv-lib"; then
  mv dist/rdv-lib dist/rdv-lib-old
fi
mv $dstDir dist/rdv-lib
if test -e "dist/rdv-lib-old"; then
  rm -rf dist/rdv-lib-old
fi

echo -e "Final\n - to: /app/rdv-ws/dist/rdv-lib"
